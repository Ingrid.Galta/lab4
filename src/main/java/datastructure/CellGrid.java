package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    cellular.CellState [][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.grid = new CellState[rows][columns];
        for(int row = 0; row < rows; row++){
            for( int col = 0; col < columns; col++){
                grid[row][col]=initialState;
            }
        }
	}

    @Override
    public int numRows() {
        
        return this.grid.length;
    }

    @Override
    public int numColumns() {
        return this.grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
       
        if (row >= 0 && row < numRows()&& column >= 0 && column < numColumns()){
            grid[row][column] = element;
        }
        else{
            throw new IndexOutOfBoundsException("Invalid row or column");
        }
    }

    @Override
    public CellState get(int row, int column) {
      
        if (row >= 0 && row < numRows()&& column >= 0 && column < numColumns()){
            return grid[row][column];
        }else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copy = new CellGrid(numRows(), numColumns(), CellState.DEAD);
        for(int row = 0; row < numRows(); row++){
            for( int col = 0; col < numColumns(); col++){
               copy.set(row, col, get(row, col)); 
            }
        }

        return copy;
    }
    
}
